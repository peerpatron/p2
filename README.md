PeerPatron
----------

PeerPatron is a crowd patronage service based on the Tezos blockchain.

It lets a crowd of patrons support creators by subscribing to small monthly
donations. It empowers the users by using a blockchain and making any kind of
centralized power abuse impossible.

A prototype is available at this address: http://peerpatron.com/

## Development

We'll publish a lot of code as free software once the development is more
advanced. For now, the following repositories are mostly private.

### Repositories

- Documentation: https://gitlab.com/peerpatron/p2
- Web client: https://gitlab.com/peerpatron/p2web
- Server: https://gitlab.com/peerpatron/p2server
